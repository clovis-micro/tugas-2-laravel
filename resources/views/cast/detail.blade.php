@extends('layouts.master')
@section('title', 'Detail Pemain Film')

@section('content')
    <h1 class="text-info">{{ $cast->nama }}</h1>
    <p>{{ $cast->bio }}</p>
    <a href="/cast" class="btn btn-primary btn-sm">Kembali</a>
@endsection
