@extends('layouts.master')
@section('title', 'Semua Pemain Film')

@section('content')
    <a href="/cast/create" class="btn btn-info my-3">Tambah Pemain film</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $keys=>$cas)
            <tr>
                <th scope="row">{{ $keys + 1 }}</th>
                <td>{{ $cas->nama }}</td>
                <td>{{ $cas->umur }}</td>
                <td>
                    <form action="/cast/{{$cas->id}}" method="post">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$cas->id}}" class="btn btn-primary btn-sm">Detail</a>
                        <a href="/cast/{{$cas->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                    </form>
                </td>
            </tr>
            @empty
                <tr>
                    <td>Cast Kosong </td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
